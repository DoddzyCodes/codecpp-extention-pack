C++ Code Extentions
===================

This package contains a small group of extentions to assist with C++ development in VSCode.

These extentions were not created by me, and all praise should go towards the owners of the individual extentions.

The following extentions are included:
    * [Cmake](https://marketplace.visualstudio.com/items?itemName=twxs.cmake)
    * [CMake Tools](https://marketplace.visualstudio.com/items?itemName=vector-of-bool.cmake-tools)
    * [CMake Tools Helper](https://marketplace.visualstudio.com/items?itemName=maddouri.cmake-tools-helper)
    * [C/C++](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cpptools)
