C++ Code Extentions
===================

This package contains a variety of extentions to assist with C++ development in VSCode.

These extentions were not created by me, and all praise should go towards the owners of the individual extentions.

The following extentions are included:
    * [Cmake](https://marketplace.visualstudio.com/items?itemName=twxs.cmake)
    * [CMake Tools](https://marketplace.visualstudio.com/items?itemName=vector-of-bool.cmake-tools)
    * [CMake Tools Helper](https://marketplace.visualstudio.com/items?itemName=maddouri.cmake-tools-helper)
    * [C/C++](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cpptools)
    * [C/C++ Clang Command Adapter](https://marketplace.visualstudio.com/items?itemName=mitaki28.vscode-clang)
    * [Clang Format](https://marketplace.visualstudio.com/items?itemName=xaver.clang-format)
    * [Git Lens](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens)
    * [Include Autocomplete](https://marketplace.visualstudio.com/items?itemName=ajshort.include-autocomplete)


